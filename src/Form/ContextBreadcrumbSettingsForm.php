<?php

namespace Drupal\context_breadcrumb\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Setting form for Context Breadcrumb.
 *
 * @package Drupal\context_breadcrumb\Form
 */
class ContextBreadcrumbSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The config key.
   */
  const CONFIG_KEY = 'context_breadcrumb.settings';

  /**
   * Config for jsonld.
   */
  const ENABLE_JSONLD = 'enable_json_ld';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'context_breadcrumb_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::CONFIG_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG_KEY);
    $form[self::ENABLE_JSONLD] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable JsonLd'),
      '#description' => $this->t('Enable render JsonLd meta tag.'),
      '#required' => FALSE,
      '#return_value' => 1,
      '#default_value' => $config->get(self::ENABLE_JSONLD),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::CONFIG_KEY)
      ->set(self::ENABLE_JSONLD, $form_state->getValue(self::ENABLE_JSONLD))
      ->save();
    Cache::invalidateTags(['render']);
  }

}
