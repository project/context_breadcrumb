<?php

namespace Drupal\context_breadcrumb\Service;

/**
 * The implement of JsonLdDataInterface.
 *
 * @package Drupal\context_breadcrumb\Service
 */
class JsonLdData implements JsonLdDataInterface {

  /**
   * The data list items.
   */
  protected array $dataListItems = [];

  /**
   * {@inheritdoc}
   */
  public function setDataListItems(array $data): void {
    $this->dataListItems = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildJsonLdData(): array {
    $i = 0;
    $jsonLdItems = [];
    foreach ($this->dataListItems as $item) {
      /** @var \Drupal\Core\Link $item */
      $dataItem = [
        '@type' => 'ListItem',
        'position' => ++$i,
        'item' => [
          'name' => $item->getText(),
        ],
      ];
      $dataItem['item']['@id'] = $item->getUrl()->setAbsolute(TRUE)->toString();
      $jsonLdItems[] = $dataItem;
    }

    if (empty($jsonLdItems)) {
      return [];
    }

    return [
      '@context' => 'http://schema.org',
      '@type' => 'BreadcrumbList',
      'itemListElement' => $jsonLdItems,
    ];

  }

}
