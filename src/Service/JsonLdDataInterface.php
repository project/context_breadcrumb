<?php

namespace Drupal\context_breadcrumb\Service;

/**
 * THe Json Ld Data.
 *
 * @package Drupal\context_breadcrumb\Service
 */
interface JsonLdDataInterface {

  /**
   * Set the links data.
   *
   * @param \Drupal\Core\Link[] $data
   *   The list of breadcrumb link.
   */
  public function setDataListItems(array $data): void;

  /**
   * Build the json ld data.
   *
   * @return array
   *   The json ld data array.
   */
  public function buildJsonLdData(): array;

}
