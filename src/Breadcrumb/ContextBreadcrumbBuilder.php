<?php

namespace Drupal\context_breadcrumb\Breadcrumb;

use Drupal\context\ContextManager;
use Drupal\context_breadcrumb\Event\ContextBreadcrumbEvent;
use Drupal\context_breadcrumb\Plugin\ContextReaction\Breadcrumb as ContextBreadcrumb;
use Drupal\context_breadcrumb\Service\JsonLdDataInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\node\Entity\Node;
use Drupal\Node\NodeStorageInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\Taxonomy\TermStorageInterface;
use Drupal\user\Entity\User;
use Drupal\User\UserStorageInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * The Context Breadcrumb Builder.
 */
class ContextBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The context breadcrumb reactions.
   */
  protected array $contextReactions;

  /**
   * The node storage.
   */
  protected NodeStorageInterface|EntityStorageInterface $nodeStorage;

  /**
   * The taxonomy storage.
   */
  protected UserStorageInterface|EntityStorageInterface $userStorage;

  /**
   * The taxonomy storage.
   */
  protected TermStorageInterface|EntityStorageInterface $termStorage;

  /**
   * The entity manager.
   */
  protected EntityTypeManagerInterface $entityManager;

  /**
   * The token.
   */
  protected Token $token;

  /**
   * The current user account.
   */
  protected AccountInterface $user;

  /**
   * The context manager.
   */
  protected ContextManager $contextManager;

  /**
   * The logger.
   */
  protected LoggerChannelFactoryInterface $logger;

  /**
   * The json ld data service.
   */
  protected JsonLdDataInterface $jsonLdData;

  /**
   * Constructs the ConextBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @param \Drupal\Core\Utility\Token $token
   *   The token.
   * @param \Drupal\context\ContextManager $contextManager
   *   The context manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityManager, AccountInterface $account, Token $token, ContextManager $contextManager, LoggerChannelFactoryInterface $logger) {
    $this->entityManager = $entityManager;
    $this->nodeStorage = $entityManager->getStorage('node');
    $this->userStorage = $entityManager->getStorage('user');
    $this->termStorage = $entityManager->getStorage('taxonomy_term');
    $this->user = $account;
    $this->token = $token;
    $this->contextManager = $contextManager;
    $this->logger = $logger;
  }

  /**
   * Validate string is token.
   *
   * @param string $str
   *   The string to validate token.
   *
   * @return bool
   *   Validate result.
   */
  public static function isToken($str) {
    return strpos($str, '[') !== FALSE || strpos($str, '{{') !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $this->contextReactions = $this->contextManager->getActiveReactions('context_breadcrumb');
    return !empty($this->contextReactions);
  }

  /**
   * Translates a string to the current language or to a given language.
   *
   * @param string $string
   *   The input string.
   * @param array $args
   *   The args.
   * @param array $options
   *   The options.
   *
   * @return string
   *   The translate markup.
   */
  protected function trans(string $string, array $args = [], array $options = []): string {
    // phpcs:ignore
    $markup = new TranslatableMarkup($string, $args, $options, $this->getStringTranslation());
    return $markup->render();
  }

  /**
   * Render data.
   *
   * @param string $title
   *   The title.
   * @param string|int $renderType
   *   The render type.
   * @param array|mixed $data
   *   Context data.
   *
   * @return mixed|string|null
   *   Title render output.
   */
  protected function renderData($title, $renderType, $data) {
    if (strpos($title, '[') !== FALSE && strpos($title, ']') !== FALSE) {
      // Render token.
      return $this->token->replace($title, $data, ['clear' => TRUE]);
    }
    if (strpos($title, '{{') !== FALSE && strpos($title, '}}') !== FALSE) {
      $render_array = [
        '#type' => 'inline_template',
        '#template' => $title,
        '#context' => $data,
      ];
      $renderer = \Drupal::service('renderer');
      return (string) $renderer->render($render_array);
    }
    return $title;
  }

  /**
   * Get entity storage instance.
   *
   * @param string $key
   *   The storage key name.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface|null
   *   The storage instance or null.
   */
  protected function getEntityStorage(string $key): ?EntityStorageInterface {
    try {
      return $this->entityManager->getStorage($key);
    }
    catch (\Exception $exception) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    if (!empty($this->contextReactions)) {
      $language = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);
      foreach ($this->contextReactions as $reaction) {
        if (empty($reaction) || !($reaction instanceof ContextBreadcrumb)) {
          continue;
        }

        /** @var \Drupal\context\ContextReactionInterface $reaction */
        $contextBreadcrumbs = $reaction->execute();
        foreach ($contextBreadcrumbs as $contextBreadcrumb) {
          try {
            if (is_string($contextBreadcrumb['title']) && mb_strlen($contextBreadcrumb['title']) > 0) {
              if ($contextBreadcrumb['url'] == '<front>') {
                $contextBreadcrumb['url'] = '/';
              }

              if (!empty($contextBreadcrumb['token'])) {
                $token_data = [];
                $params = $route_match->getParameters();
                foreach ($params->keys() as $key) {
                  $param_object = $params->get($key);
                  if ($key === 'node' && !is_object($param_object)) {
                    $param_object = Node::load($param_object);
                  }
                  if ($key === 'user' && !is_object($param_object)) {
                    $param_object = User::load($param_object);
                  }
                  if ($key === 'node_revision' && !is_object($param_object)) {
                    $param_object = $this->entityManager
                      ->getStorage('node')
                      ->loadRevision($param_object);
                  }
                  if (!is_object($param_object) && $storage = $this->getEntityStorage($key)) {
                    $param_object = $storage->load($param_object);
                  }

                  $token_data[$key] = $param_object;
                }

                /** @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher */
                $contexts = array_map(function ($context) {
                  return $context->id();
                }, $this->contextManager->getActiveContexts());

                $event_dispatcher = \Drupal::service('event_dispatcher');
                $event = new GenericEvent();
                $event->setArgument('token_data', $token_data);
                $event->setArgument('contexts', $contexts);
                $event_dispatcher->dispatch($event, ContextBreadcrumbEvent::TOKEN_DATA);
                $token_data = $event->getArgument('token_data');

                if ($this->checkHierarchyToken($contextBreadcrumb['title'], $contextBreadcrumb['url'])) {
                  $hierarchy_arr = explode(':', str_replace(['[', ']'], '', $contextBreadcrumb['url']));
                  if (count($hierarchy_arr) === 3 && $entity = $params->get($hierarchy_arr[1])) {
                    $field_name = $hierarchy_arr[2];
                    if ($entity instanceof EntityInterface) {
                      $breadcrumb->addCacheableDependency($entity);
                      if ($entity->hasField($field_name)) {
                        $term = $entity->get($field_name)->entity;
                        if ($term instanceof TermInterface) {
                          $parents = $this->getAllParents($term->id());
                          foreach (array_reverse($parents) as $parent) {
                            $link = $parent->toLink($parent->label());
                            $breadcrumb->addLink($link);
                            $breadcrumb->addCacheableDependency($parent);
                          }
                        }
                      }
                    }
                  }
                  continue;
                }

                $contextBreadcrumb['title'] = $this->renderData($contextBreadcrumb['title'], $contextBreadcrumb['token'], $token_data);
                if (mb_strlen($contextBreadcrumb['title']) > 0) {
                  if ($contextBreadcrumb['url'] === '<nolink>') {
                    $contextBreadcrumb['url'] = Url::fromRoute($contextBreadcrumb['url']);
                  }
                  else {
                    $contextBreadcrumb['url'] = $this->renderData($contextBreadcrumb['url'], $contextBreadcrumb['token'], $token_data);
                  }

                  if ($contextBreadcrumb['url'] instanceof Url) {
                    $breadcrumb->addLink(Link::fromTextAndUrl($this->trans($contextBreadcrumb['title']), $contextBreadcrumb['url']));
                  }

                  if (!empty($contextBreadcrumb['url']) && is_string($contextBreadcrumb['url'])) {
                    if (strpos($contextBreadcrumb['url'], '://') !== FALSE) {
                      $breadcrumb->addLink(Link::fromTextAndUrl($this->trans($contextBreadcrumb['title']), Url::fromUri($contextBreadcrumb['url'])));
                    }
                    else {
                      $breadcrumb->addLink(Link::fromTextAndUrl($this->trans($contextBreadcrumb['title']), Url::fromUserInput($contextBreadcrumb['url'])));
                    }
                  }
                }
              }
              else {
                $url = $contextBreadcrumb['url'] === '<nolink>' ? Url::fromRoute($contextBreadcrumb['url']) : Url::fromUserInput($contextBreadcrumb['url'], ['language' => $language]);
                $breadcrumb->addLink(Link::fromTextAndUrl($this->trans($contextBreadcrumb['title']), $url));
              }
            }
          }
          catch (\Exception $e) {
            $this->logger->get('context_breadcrumb')->error($e->getMessage());
          }
        }
      }
    }

    $params = $route_match->getParameters()->all();
    foreach ($params as $param) {
      if ($param instanceof CacheableDependencyInterface) {
        $breadcrumb->addCacheableDependency($param);
      }
    }
    $breadcrumb->addCacheContexts(['url']);
    $breadcrumb->addCacheContexts(['url.site']);
    $breadcrumb->addCacheContexts(['url.query_args']);
    $breadcrumb->addCacheTags(['context:breadcrumb']);

    return $breadcrumb;
  }

  /**
   * Check is hierarchy Token.
   *
   * @param string $title
   *   The title.
   * @param string $url
   *   The url.
   *
   * @return bool
   *   Check result.
   */
  protected function checkHierarchyToken(string $title, string $url): bool {
    if ($title === '[term_hierarchy]' && str_contains($url, '[term_hierarchy:')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get term parents.
   *
   * @param int $tid
   *   Term id.
   *
   * @return mixed
   *   List of entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllParents($tid) {
    return $this->entityManager->getStorage("taxonomy_term")
      ->loadAllParents($tid);
  }

  /**
   * Set the json ld data service.
   *
   * @param \Drupal\context_breadcrumb\Service\JsonLdDataInterface $jsonLdData
   *   The json ld data service.
   */
  public function setJsonLdData(JsonLdDataInterface $jsonLdData): void {
    $this->jsonLdData = $jsonLdData;
  }

}
