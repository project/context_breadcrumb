<?php

namespace Drupal\context_breadcrumb\Event;

/**
 * Context breadcrumb event.
 *
 * @package Drupal\context_breadcrumb\Event
 */
class ContextBreadcrumbEvent {

  /**
   * Custom token data event.
   */
  const TOKEN_DATA = 'context_breadcrumb_token_data';

}
