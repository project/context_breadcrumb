<?php

namespace Drupal\context_breadcrumb\Plugin\ContextReaction;

use Drupal\context\ContextReactionPluginBase;
use Drupal\context_breadcrumb\Breadcrumb\ContextBreadcrumbBuilder;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a content reaction that adds breadcrumb to page.
 *
 * @ContextReaction(
 *   id = "context_breadcrumb",
 *   label = @Translation("Breadcrumb")
 * )
 */
class Breadcrumb extends ContextReactionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $permission = [
      'breadcrumbs' => [],
    ];

    return parent::defaultConfiguration() + $permission;
  }

  /**
   * {@inheritdoc}
   */
  public function summary(): TranslatableMarkup {
    return $this->t('Context breadcrumb');
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this->getConfiguration()['breadcrumbs'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $breadcrumbs = $this->getConfiguration()['breadcrumbs'];
    $form['breadcrumbs'] = [
      '#type' => 'table',
      '#header' => [
        '',
        $this->t('Title'),
        $this->t('Url'),
        $this->t('Token'),
        $this->t('Weight'),
      ],
      '#rows' => [],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];

    for ($i = 0; $i <= 8; $i++) {
      // Mark the table row as draggable.
      $form['breadcrumbs'][$i]['#attributes']['class'][] = 'draggable';
      // Sort the table row according to its existing/configured weight.
      $form['breadcrumbs'][$i]['#weight'] = $breadcrumbs[$i]['weight'] ?? $i;

      $form['breadcrumbs'][$i]['drag'] = [
        '#markup' => ' ',
      ];

      $form['breadcrumbs'][$i]['title'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Title'),
        '#rows' => 1,
        '#title_display' => 'invisible',
        '#default_value' => $breadcrumbs[$i]['title'] ?? '',
      ];
      $form['breadcrumbs'][$i]['url'] = [
        '#type' => 'textarea',
        '#title' => $this->t('URL'),
        '#rows' => 1,
        '#title_display' => 'invisible',
        '#default_value' => $breadcrumbs[$i]['url'] ?? '',
      ];

      $form['breadcrumbs'][$i]['token'] = [
        '#type' => 'select',
        '#required' => FALSE,
        '#title' => $this->t('Token'),
        '#title_display' => 'invisible',
        '#default_value' => !empty($breadcrumbs[$i]['token']) ? 1 : '',
        '#empty_value' => '',
        '#options' => [
          '' => $this->t('None'),
          1 => $this->t('Yes'),
        ],
      ];

      $form['breadcrumbs'][$i]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for this row'),
        '#title_display' => 'invisible',
        '#default_value' => !empty($breadcrumbs[$i]['weight']) ? $breadcrumbs[$i]['weight'] : $i,
        // Classify the weight element for #tabledrag.
        '#attributes' => ['class' => ['table-sort-weight']],
      ];
    }

    if (\Drupal::service('module_handler')->moduleExists("token")) {
      $token_tree = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['node', 'user', 'term', 'vocabulary'],
      ];

      $rendered_token_tree = \Drupal::service('renderer')->render($token_tree);
      $form['description']['#type'] = 'item';
      $form['description']['#description'] = t('This field supports tokens. @browse_tokens_link', [
        '@browse_tokens_link' => $rendered_token_tree,
      ]);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    foreach ($form_state->getValue('breadcrumbs') as $i => $breadcrumb) {
      if (mb_strlen($breadcrumb['title']) && !mb_strlen($breadcrumb['url'])) {
        $form_state->setErrorByName('breadcrumbs][' . $i . '][url', $this->t('@name field is required.', ['@name' => 'Url']));
      }
      if (!mb_strlen($breadcrumb['title']) && mb_strlen($breadcrumb['url'])) {
        $form_state->setErrorByName('breadcrumbs][' . $i . '][token', $this->t('@name field is required.', ['@name' => 'Token']));
      }

      if (mb_strlen($breadcrumb['title']) && mb_strlen($breadcrumb['url'])) {
        if (ContextBreadcrumbBuilder::isToken($breadcrumb['url']) && empty($breadcrumb['token'])) {
          $form_state->setErrorByName('breadcrumbs][' . $i . '][token', $this->t('The url using token, please select token option.'));
        }
        if (ContextBreadcrumbBuilder::isToken($breadcrumb['title']) && empty($breadcrumb['token'])) {
          $form_state->setErrorByName('breadcrumbs][' . $i . '][token', $this->t('The title using token, please select token option.'));
        }
        if (!ContextBreadcrumbBuilder::isToken($breadcrumb['url'])
          && !in_array($breadcrumb['url'], ['<front>', '<nolink>'])
          && !str_contains($breadcrumb['url'], 'http://')
          && !str_contains($breadcrumb['url'], 'https://')
          && $breadcrumb['url'][0] !== '/') {
          $form_state->setErrorByName('breadcrumbs][' . $i . '][url', $this->t('The url path has to start with a slash.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $breadcrumbs = $form_state->getValue('breadcrumbs');

    // Re-order the crumbs based on the weight's value.
    usort($breadcrumbs, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return $a['weight'] < $b['weight'] ? -1 : 1;
    });

    // Update the weight values to be consistent to the index.
    for ($i = 0; $i <= 8; $i++) {
      $breadcrumbs[$i]['weight'] = $i;
    }

    // Update the indexes.
    $breadcrumbs = array_values($breadcrumbs);

    $form_state->setValue('breadcrumbs', $breadcrumbs);

    $this->setConfiguration([
      'breadcrumbs' => $form_state->getValue('breadcrumbs'),
    ]);
    Cache::invalidateTags(['context:breadcrumb']);
  }

}
