<?php

namespace Drupal\context_breadcrumb\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\context_breadcrumb\Form\ContextBreadcrumbSettingsForm;
use Drupal\context_breadcrumb\Service\JsonLdDataInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * The class handler response subscriber.
 */
class ResponseSubscriber implements EventSubscriberInterface {

  /**
   * The route match service.
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The json ld data service.
   */
  protected JsonLdDataInterface $jsonLdData;

  /**
   * The config.
   */
  protected ImmutableConfig $config;

  /**
   * Set route match service.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The  route match service.
   */
  public function setRouteMatch(RouteMatchInterface $route_match): void {
    $this->routeMatch = $route_match;
  }

  /**
   * Set the json ld data service.
   *
   * @param \Drupal\context_breadcrumb\Service\JsonLdDataInterface $jsonLdData
   *   The json ld data service.
   */
  public function setJsonLdData(JsonLdDataInterface $jsonLdData): void {
    $this->jsonLdData = $jsonLdData;
  }

  /**
   * Set config object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function setConfig(ConfigFactoryInterface $configFactory): void {
    $this->config = $configFactory->get(ContextBreadcrumbSettingsForm::CONFIG_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE] = ['onResponse'];

    return $events;
  }

  /**
   * This method is called the KernelEvents::RESPONSE event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function onResponse(ResponseEvent $event) {
    if (!$this->routeMatch->getRouteObject()) {
      return;
    }
    if ($this->routeMatch->getRouteObject()->getOption('_admin_route')) {
      return;
    }
    if (!$this->config->get(ContextBreadcrumbSettingsForm::ENABLE_JSONLD)) {
      return;
    }

    $content = $event->getResponse()->getContent();
    $find_str = '<script type="context_breadcrumb_ld"></script>';
    if ($content && str_contains($content, $find_str)) {
      $replace = '';
      $json_ld = $this->jsonLdData->buildJsonLdData();
      if (!empty($json_ld)) {
        $json_ld = Json::encode($json_ld);
        $replace = "<script type='application/ld+json'>$json_ld</script>";
      }
      $content = str_replace($find_str, $replace, $content);
      $event->getResponse()->setContent($content);
    }
  }

}
